
# Проверка последней версии на сервере redis 
export latest_version_redis=$(echo "hget version latest" | redis-cli -a lolkek123)
# Проверка последней версии на s3
export latest_version_s3=$(mc ls minio-vm-2/search-indices/faiss_flat_index/ | awk '$5>x{x=$5};END{print x}' | egrep -o '[0-9.]+'
)


# При несовпадении версий, происходит запуск скрипта скачивания с s3 в нужные директории
if [ "$latest_version_redis" != "$latest_version_s3" ]; then
  echo "Начало обновления модели" && \
  # Загрузка данных в /var/models на vm-2, затем на vm-1
  mc cp --recursive minio-vm-2/search-indices/ /var/models && \
  ssh vm-1 "mc cp --recursive minio-vm-2/search-indices/ /var/models" && \
  # Загрузка данных в /var/models на vm-2, затем на vm-1
  mc cp --recursive minio-vm-2/encoders/ /var/models && \
  ssh vm-1 "mc cp --recursive minio-vm-2/encoders/ /var/models" && \
  echo "Конец обновления" && \
  echo "Запуск обновления образа qa-app" && \
  # Создание + заливка + обновление сервиса на актуальной версию образа + путем до файлов с моделями
  cd ~/qa-app && docker build --build-arg model_version=$latest_version_s3 -t 65.108.158.255:5000/qa-app:$latest_version_s3 . && \
  docker push 65.108.158.255:5000/qa-app:$latest_version_s3 && \
  docker service update --image 65.108.158.255:5000/qa-app:$latest_version_s3 qa-service && \
  echo "Конец обновления qa-app"
else
  echo "Модель последней версии"
fi

# Проверка обновления сервиса (в случае ролл-бэка будет старая версия)
export version_on_prod=$(curl http://95.217.183.140:333/ | egrep -o '[0-9.]+')


# Обновления статусов версий в redis
if [ "$version_on_prod" != "$latest_version_redis" ]; then
  echo "hset version previous $latest_version_redis" | redis-cli -a lolkek123 && \
  echo "hset version latest $latest_version_s3" | redis-cli -a lolkek123 
else
  echo "Модель не обновилась"
fi
